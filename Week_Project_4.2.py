class MySet:
    def __init__(self):
        self.elements = []

    def add(self, x):
        if x not in self.elements:
            index = 0
            while index < len(self.elements) and self.elements[index] < x:
                index += 1
            self.elements.insert(index, x)

    def remove(self, x):
        if x in self.elements:
            self.elements.remove(x)

    def contains(self, x):
        return x in self.elements


my_set = MySet()
my_set.add(10)
my_set.add(20)
my_set.add(5)
print(my_set.contains(10))
print(my_set.contains(15))
print(my_set.contains(20))
my_set.remove(20)
print(my_set.contains(20))
